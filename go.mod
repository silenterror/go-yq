module gitlab.com/silenterror/yq

go 1.13

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
