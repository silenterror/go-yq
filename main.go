package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/go-yaml/yaml"
)

func main() {
	// obtain stdin
	input := os.Stdin

	// stat stdin as it should be an open file returning infomation about the file
	finfo, err := input.Stat()
	if err != nil {
		log.Fatal(err)
	}

	// check in stdin is valid
	err = checkStdin(finfo)
	if err != nil {
		log.Fatal(err)
	}

	// create a bufio reader and read stdin input has a method Read which satisfies
	// the New Reader requirements
	reader := bufio.NewReader(input)

	// create the var for the output of type rune which will hold each character
	// from std
	var output []rune

	// read each rune from our reader which contains the input
	for {
		i, _, err := reader.ReadRune()
		// check err and check in EOF end of file was encountered
		if err != nil && err == io.EOF {
			break
		}
		// append each rune to the []rune
		output = append(output, i)
	}

	// iterate over output and print the characters one after another
	for i := 0; i < len(output); i++ {
		fmt.Printf("%c", output[i])
	}

	// cast the []rune as a string thus joining them together then case the string
	// as an []byte which we can unmarshal
	b := []byte(string(output))

	// holds the yml data
	var ymlData interface{}

	// unmarshal the []byte into ymlData
	err = yaml.Unmarshal(b, &ymlData)

	fmt.Println(ymlData)

}

// checkStdin - checks to see if the Stdin is set correctly
func checkStdin(f os.FileInfo) error {
	// check if the file Mode and ModeNamedPipe are set detecting piped input
	// is waiting to be read from the std file descriptor
	if f.Mode()&os.ModeNamedPipe <= 0 {
		return errors.New("piped input not detected")
	}

	return nil
}
